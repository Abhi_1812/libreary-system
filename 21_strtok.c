#include <stdio.h>
#include <string.h>

int main()
{
	char str[] = "this,is,an,example,string";
	char *ptr;
	ptr = strtok(str, ",");
	printf("token: %s\n", ptr);
	do {
		ptr = strtok(NULL, ",");
		printf("token: %s\n", ptr);
	} while(ptr != NULL);
	printf("\n\n");

	char string[] = "ls -l -a /home";
	char* args[32];
	int i;

	i = 0;
	ptr = strtok(string, " ");
	args[i++] = ptr;
	do {
		ptr = strtok(NULL, " ");	
		args[i++] = ptr;
	}while(ptr!=NULL);

	for(i=0; args[i]!=NULL; i++)
		printf("token: %s\n", args[i]);
	return 0;	
}








