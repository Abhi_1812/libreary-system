#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int count = 0;

void* increment(void *param)
{
	int i;
	for(i=1; i<=10; i++)
	{
		printf("inc : %d\n", ++count);
		sleep(1);
	}
	return NULL;
}

void* decrement(void *param)
{
	int i;
	for(i=1; i<=10; i++)
	{
		printf("dec : %d\n", --count);
		sleep(1);
	}
	return NULL;
}

int main()
{
	int i;
	pthread_t it, dt;
	pthread_create(&it, NULL, increment, NULL);
	pthread_create(&dt, NULL, decrement, NULL);
	pthread_join(it, NULL);
	pthread_join(dt, NULL);
	printf("final count = %d\n", count);
	return 0;	
}





