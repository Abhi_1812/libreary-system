#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#define SERVER_IP	"172.18.7.196"
#define SERVER_PORT	3000

int main()
{
	char msg[64];
	int srv_fd, cli_fd;
	struct sockaddr_in srv_addr, cli_addr;
	socklen_t len = sizeof(srv_addr);
	//1. create server socket
	srv_fd = socket(AF_INET, SOCK_STREAM, 0);
	printf("server socket created.\n");

	//2. assign address to server socket
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(SERVER_PORT);
	inet_aton(SERVER_IP, &srv_addr.sin_addr);
	bind(srv_fd, (struct sockaddr*)&srv_addr, len);
	printf("address given to server socket.\n");

	//3. listen to server socket
	listen(srv_fd, 5);
	printf("listening to server socket.\n");

	//6. accept client connection
	printf("waiting for client connection.\n");
	cli_fd = accept(srv_fd, (struct sockaddr*)&cli_addr, &len);
	printf("client connection connection accepted.\n");

	do {
		//8. read data from client & display
		read(cli_fd, msg, sizeof(msg));
		printf("client: %s", msg);

		//9. write data to client
		printf("server: ");
		fgets(msg, sizeof(msg), stdin);
		write(cli_fd, msg, strlen(msg)+1);
	} while(strcmp(msg, "bye\n")!=0);

	//12. close socket
	close(cli_fd);
	printf("client socket closed.\n");

	//13. shutdown listening socket
	shutdown(srv_fd, SHUT_RDWR);
	printf("server socket shutdown.\n");
	return 0;	
}

