#include ""
void menudriven_lib()
{
	int choice = 0;
	do {
		choice = menu();
		switch(choice)
		{
		case 0: sign_out();
			break;
		case 1: edit_profile();
			break;
		case 2: change_password();
			break;
		case 3: find_book_by_name();
			break;
		case 4: check_book_availability();
			break;
		case 5: add_new_book();
			break;
		case 6: add_new_copy();
			break;
		case 7: issue_book_copy();
			break;
		case 8: return_book_copy();
			break;
		case 9: list_issued_copy();
			break;
		case 10: edit_book();
			break;
		case 11: change_rack();
			break;
		case 12: add_member();
			break;
		case 13: take_payment();
			break;
		case 14: payment_history();
			break;
		}
	}while(1);
	return 0;
}

int menu()
{
	int choice;
	printf("\n 0. Sign out\n 1. edit profile\n 2. change password\n 3. find book by name\n 4. check book availability\n 5. add new book\n 6.add new copy\n 7. issue book copy\n 8. return book entry\n 9. list issued book\n 10. edit book\n 11. change book \n 12. add member\n 13. take payment\n 14. payment history\n Enter Choice : ");
	scanf("%d",choice);
	return choice;
}
