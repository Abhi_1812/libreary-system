void (emp_t *e)
{
	printf("name: ");
	scanf("%s", e->name);
	printf("author : ");
	scanf("%s", e->author);
	printf("subject: ");
	scanf("%s", &e->subject);
	printf("price: ");
	scanf("%f", &e->price);
	printf("ISBN: ");
	scanf("%d", &e->isbn);
}


void add_books()
{
	emp_t e;
	int fd;

	accept_emp(&e);

	fd = open(EMP_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open emp file");
		_exit(1);
	}
	write(fd, &e, sizeof(e));

	close(fd);

}
