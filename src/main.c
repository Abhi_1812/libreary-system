#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
char id[20];
#define SERVER_IP	"172.18.4.119"
#define SERVER_PORT	3000

void welcomeMessage()
{
    printf("\n\n\n\n\n");
    printf("\n\t\t\t  **-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**\n");
    printf("\n\t\t\t        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    printf("\n\t\t\t        =                 WELCOME                   =");
    printf("\n\t\t\t        =                   TO                      =");
    printf("\n\t\t\t        =                 LIBRARY                   =");
    printf("\n\t\t\t        =               MANAGEMENT                  =");
    printf("\n\t\t\t        =                 SYSTEM                    =");
    printf("\n\t\t\t        =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    printf("\n\t\t\t  **-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**-**\n");
    printf("\n\n\n\t\t\t Enter any key to continue.....");
    getchar();
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
}

int main()
{
	char msg[64];
	int cli_fd;
	int i;
	int choice;
 	char id[20],pass[20],ser[30]="",*args[32],*ptr,c;
	struct sockaddr_in srv_addr;
	socklen_t len = sizeof(srv_addr);

	welcomeMessage();

	//4. create client socket
	cli_fd = socket(AF_INET, SOCK_STREAM, 0);
	printf("client socket created.\n");

	//5. connect to server socket
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(SERVER_PORT);
	inet_aton(SERVER_IP, &srv_addr.sin_addr);
	connect(cli_fd, (struct sockaddr*)&srv_addr, len);
	printf("connected to server socket.\n");


	printf("\n0.exit\n1.signin\n2.signup\n");
	scanf("%d",&choice);
 	switch(choice)
 	{
  	 case 0: exit(0);
	 	 break;
  	 case 1: printf("\nEnter Email Id:");
         	 scanf("%s",id);
	 	 printf("\nEnter Password:");
		 scanf("%s",pass);
	  	 strcat(ser,"sin/");
	  	 strcat(ser,id);
	  	 strcat(ser,"/");
	  	 strcat(ser,pass);
	 	 break;
	  
     	 case 2: printf("\nEnter Email Id:");
         	 scanf("%s",id);
	 	 printf("\nEnter Password:");
	 	 scanf("%s",pass);
	 	 strcat(ser,"sup/");
	 	 strcat(ser,id);
	 	 strcat(ser,"/");
	 	 strcat(ser,pass);
	 	 break;	
	}
	
	//7. send data to server
	printf("\nSending data to server:%s",ser);
	write(cli_fd, ser, strlen(ser)+1);
	printf("\nData sent sucessfully");

	//10. read data from server & display
	read(cli_fd, msg, sizeof(msg));
	printf("\nrecived string: %s",msg);
	
	i = 0;
	ptr = strtok(msg, "/");
	args[i++] = ptr;
	do {
		ptr = strtok(NULL, " ");	
		args[i++] = ptr;
	} while(ptr!=NULL);

	if(!strcmp(args[0],"fail"))
	{
	 printf("\nInvalid User!");
	}
	if(!strcmp(args[0],"success"))
	{
	 printf("\nLogged in Sucessfull!");
	}
	/*switch(c)
	{
	 case 1:owner();break;
	 case 2:lib();break;
	 case 3:member();break;
	}
	*/
	//11. close client socket
	close(cli_fd);

	return 0;
}
