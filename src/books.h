#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define BOOK_FILE "book.txt"

typedef struct book
{
	char id[2];
	char userid[10];
	char name[40];
	char author[40];
	char subject[40];
	char price[5];
	char isbn[10];
	char acs[50];
}book_t;

int menu();
int accept_book(book_t *b);
int print_book(book_t *b);
int add_book(char *id);
int edit_book(char *id);
int print_all_books();
book_t* find_book(char*,book_t*);
int delete_book(char *id);

/*
int print_book(book_t *b)
{
	printf("%d\n%s\n%s\n%s\n%f\n%d\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}

int add_book(char *id)
{
	book_t b;
	int fd;
	// accept a new book record from user
	accept_book(&b);
	// open file for appending into the file
	fd = open(BOOK_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open book file");
		_exit(1);
	}

	// write book record into the file
	write(fd, &b, sizeof(b));

	// close file
	close(fd);
}

int print_all_books()
{
	int fd;
	book_t b;

	// open file for reading
	fd = open(BOOK_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open book file");
		_exit(1);
	}

	// read a book record
	while( read(fd, &b, sizeof(b)) > 0 )
	{
		// display book record
		print_book(&b);
	} // repeat until end of file is reached

	// close file
	close(fd);
}
*/
book_t* find_book(char *name,book_t* book)
{
	int fd, found=0;
	book_t b;

	// open file for reading
	fd = open(BOOK_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open book file");
		_exit(1);
	}

	// read a book record
	while( read(fd, &b, sizeof(b)) > 0 )
	{
		if(!strcmp(b.name,name))
		{
			// display book record
			book = &b;
			close(fd);
			return book;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);
}
/*
int edit_book(char *id)
{
	int fd, found=0;
	book_t b, nb;
	long size = sizeof(struct book);

	// open file for reading
	fd = open(BOOK_FILE, O_RDWR);
	if(fd < 0)
	{
		perror("failed to open book file");
		_exit(1);
	}

	printf("enter id to be searched: ");
	scanf("%d", &id);

	// read a book record
	while( read(fd, &b, sizeof(b)) > 0 )
	{
		if(b.id == id)
		{
			// display book record
			print_book(&b);
			found = 1;

			// enter new details
			accept_book(&nb);

			// change cur pos to one record back
			lseek(fd, -size, SEEK_CUR);

			// over-write new details
			write(fd, &nb, sizeof(nb));
			break;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);

	if(found == 0)
		printf("book record not found.\n");
}

int delete_book(char *id)
{
	int fd, fs, found = 0;
	book_t b;

	// open book file for reading
	fs = open(BOOK_FILE, O_RDONLY);
	if(fs < 0)
	{
		perror("failed to open book file");
		_exit(1);
	}

	printf("enter id to be deleted: ");
	scanf("%d", &id);

	// create new temp file for writing
	fd = open("temp.txt", O_WRONLY | O_TRUNC | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open book file");
		close(fs);
		_exit(1);
	}

	// read a record from book file
	while( read(fs, &b, sizeof(b)) > 0 )
	{
		if(b.id == id)
			found = 1;
		else
		{
			// write it into temp file, if it is not to be deleted
			write(fd, &b, sizeof(b));
		}
	} // repeat until end of user file

	// close temp file
	close(fd);
	// close book file
	close(fs);

	if(found == 0)
		printf("book record not found.\n");

	// delete user file
	unlink(BOOK_FILE);
	// rename temp file to user file
	link("temp.txt", BOOK_FILE);
	unlink("temp.txt");
}
*/

