void (emp_t *e)
{
	printf("id: ");
	scanf("%d", &e->id);
	printf("rack no : ");
	scanf("%d", &e->no);
}


void add_copy()
{
	emp_t e;
	int fd;

	accept_emp(&e);

	fd = open(EMP_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open emp file");
		_exit(1);
	}
	write(fd, &e, sizeof(e));

	close(fd);

}
