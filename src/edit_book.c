
void edit_book()
{
	int fd, found=0;
	char name[30];
	book_t e, ne;
	long size = sizeof(struct book);

	fd = open(BOOK_FILE, O_RDWR);
	if(fd < 0)
	{
		perror("failed to open book file");
		_exit(1);
	}

	printf("book name : ");
	scanf("%s",name);

	while( read(fd, &e, sizeof(e)) > 0 )
	{
		if(strcmp(e.name,name) == 0)
		{
			print_book_record(&e);
			found = 1;

	
			accept_book_record(&ne);

		
			lseek(fd, -size, SEEK_CUR);

	
			write(fd, &ne, sizeof(ne));
			break;
		}
	} 
	

	
	close(fd);

	if(found == 0)
		printf("book record not found.\n");
}
void accept_book_record(book_t *e)
{
	printf("name : ");
	scanf("%s", e->name);
	printf("author : ");
	scanf("%s", e->author);
	printf("subject : ");
	scanf("%s", e->subject);
	printf("price : ");
	scanf("%f", &e->price);
	printf("isbn : ");
	scanf("%ld", &e->isbn);
}

void print_book_record(book_t *e)
{
	printf("%d\t%s\t%s\t%s\t%f\t%ld\n", e->id, e->name,e->author,e->subject,e->price,e->isbn);
}



