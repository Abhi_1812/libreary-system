#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define USER_FILE "user.txt"

int userID = 20010;

typedef struct user
{
	char id[20];
	char name[30];
	char email[40];
	char phone[10];
	char pass[8];
	char role[10];
	char userid[8];
	char acs[10];
}user_t;

int menu();
void accept_user(user_t *p);
void print_user(user_t *p);
int add_user(char*,char*);
int edit_user(char*,char*,char*);
void print_all_users();
user_t* find_user(char*,char*,user_t*);
void delete_user();
int payment(int choice);

int add_user(char* id, char* pass)
{
	user_t p;
	int fd;

	strcpy(p.email,id);
	strcpy(p.pass,pass);
	sprintf(p.userid,"%d",userID++);

	fd = open(USER_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open user file");
		_exit(1);
	}

	// write user record into the file
	write(fd, &p, sizeof(p));

	// close file
	close(fd);
	
	return 0;
}


user_t* find_user(char* id, char* pass,user_t* res)
{
	int fd, found=0;
	user_t p;

	// open file for reading
	fd = open(USER_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open user file");
		_exit(1);
	}

	printf("insde sin pthread_create\n");

	// read a user record
	while( read(fd, &p, sizeof(p)) > 0 )
	{
		if(!strcmp(id,p.email))
		{
			// display user record
			printf("insde sin-pass pthread_create\n");
			if(!strcmp(pass,p.pass)){
				*res = p;				
				return res;
			}
			else 
				return NULL;		
		}
	} // repeat until end of file is reached

	// close file
	close(fd);
}

int edit_pass(char* id, char* pass)
{
	int fd, found=0;
	user_t p;
	long size = sizeof(struct user);

	// open file for reading
	fd = open(USER_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open user file");
		_exit(1);
	}

	// read a user record
	while( read(fd, &p, sizeof(p)) > 0 )
	{
		if(!strcmp(id,p.userid))
		{
			// display user record
			strcpy(p.pass,pass);
			// change cur pos to one record back
			lseek(fd, -size, SEEK_CUR);
			write(fd, &p, sizeof(p));

			close(fd);
			return 0;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);
}






int edit_user(char* id,char* email, char* phone)
{
	int fd, found=0;
	user_t p, np;
	long size = sizeof(struct user);

	// open file for reading
	fd = open(USER_FILE, O_RDWR);
	if(fd < 0)
	{
		perror("failed to open user file");
		_exit(1);
	}


	// read a payment record
	while( read(fd, &p, sizeof(p)) > 0 )
	{
		if(!strcmp(id,p.userid))
		{
			
			strcpy(np.email,email);
			strcpy(np.phone,phone);

			// change cur pos to one record back
			lseek(fd, -size, SEEK_CUR);

			// over-write new details
			write(fd, &np, sizeof(np));
			
			close(fd);

			return 0;

		}
	} // repeat until end of file is reached

	// close file
	close(fd);
}


