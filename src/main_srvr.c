#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "users.h"
#include "books.h"
#include "copies.h"
//#include "issuerecord.h"

#define SERVER_IP	"172.18.4.119"
#define SERVER_PORT	3000

pthread_t tid[100];
int t = 0;
int flag = 0;
int srv_fd, cli_fd;

typedef struct result{

	char acs[5];
	char role[10];

}result_t;


typedef struct creds{

	char action[10];
	char id[20];
	char pass[10];
	char acs[5];
	char role[10];
}cred_t;

cred_t *cred;
pthread_mutex_t m;

void handleaction(char** action);
void MemOp(char** act);


void* signup(void *param){

	int ret;

	printf("insde sun pthread_create\n");
	user_t *key = (user_t*)param;

	printf("id : %s\n",key->id);
	printf("pass : %s\n",key->pass);
	ret = add_user(key->id,key->pass);

	if(ret == 0)
		strcpy(key->acs,"success/member");
	else
		strcpy(key->acs,"failed");

	printf("Thread : >>>>%s<<<<\n",key->acs);	

	pthread_exit(key);
}

void* signin(void *param){

	printf("insde sin pthread_create\n");
	user_t *key = (user_t*)param;

	user_t *res = (user_t*)malloc(sizeof(user_t));

	printf("id : %s\n",key->id);
	printf("pass : %s\n",key->pass);
	res = find_user(key->id,key->pass,res);

	if(res != NULL){
		char response[50]="log_in_success/member/";
		
		printf("insde sin pthread_create\n");
		strcat(response,res->userid);
		strcpy(key->acs,response);
		printf("acs: %s -- response : %s ----- userid : %s\n",key->acs,response,res->userid);
		
	}
	else
		strcpy(key->acs,"log_in failed");

	printf("Thread : >>>>%s<<<<\n",key->acs);

	pthread_exit(key);
}

void* edit_profile(void *param){

        int ret;

        printf("insde E_P pthread_create\n");
        user_t *key = (user_t*)param;

	ret = edit_user(key->userid,key->email,key->phone);

        if(ret == 0)
                strcpy(key->acs,"edit success");
        else
                strcpy(key->acs,"edit failed");

        printf("Thread : >>>>%s<<<<\n",key->acs);

        pthread_exit(key);


}

void* edit_passwrd(void *param){

        int ret;

        printf("insde C_P pthread_create\n");
        user_t *key = (user_t*)param;

        ret = edit_pass(key->userid,key->pass);

        if(ret == 0)
                strcpy(key->acs,"edit success");
        else
                strcpy(key->acs,"edit failed");

        printf("Thread : >>>>%s<<<<\n",key->acs);

        pthread_exit(key);


}

void* find_book_by_name(void *param){

        int ret;

	book_t *book = (book_t*)malloc(sizeof(book_t));

        printf("insde F_B_B_N pthread_create\n");
        book_t *key = (book_t*)param;

        book = find_book(key->name,book);

        if(book != NULL){
                strcpy(key->acs,book->id);
                strcpy(key->acs,book->author);
		strcat(key->acs,book->subject);
		strcat(key->acs,book->price);
		strcat(key->acs,book->isbn);
	}
        else
                strcpy(key->acs,"edit failed");

        printf("Thread : >>>>%s<<<<\n",key->acs);

        pthread_exit(key);


}


void* check_book_avail(void *param){

        int ret;

        copy_t* copy = (copy_t*)malloc(sizeof(copy_t));

        printf("insde F_B_B_N pthread_create\n");
        copy_t *key = (copy_t*)param;

        copy = find_copy(key->book_id,copy);

        if(copy != NULL){
                strcpy(key->acs,copy->book_id);
                strcpy(key->acs,copy->rack);
                strcat(key->acs,copy->status);
        }
        else
                strcpy(key->acs,"edit failed");

        printf("Thread : >>>>%s<<<<\n",key->acs);

        pthread_exit(key);
}





int main()
{
	char msg[64];
	struct sockaddr_in srv_addr, cli_addr;
	char *ptr;
	char* args[32];
	pthread_t tid[10];
	result_t res;
	int i = 0;

	socklen_t len = sizeof(srv_addr);
	//1. create server socket
	srv_fd = socket(AF_INET, SOCK_STREAM, 0);
	printf("server socket created.\n");

	//2. assign address to server socket
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(SERVER_PORT);
	inet_aton(SERVER_IP, &srv_addr.sin_addr);
	bind(srv_fd, (struct sockaddr*)&srv_addr, len);
	printf("address given to server socket.\n");

	//3. listen to server socket
	listen(srv_fd, 5);
	printf("listening to server socket.\n");
	
	pthread_mutex_init(&m, NULL);


	//6. accept client connection
	printf("waiting for client connection.\n");
	cli_fd = accept(srv_fd, (struct sockaddr*)&cli_addr, &len);
	printf("client connection connection accepted.\n");

	while(1){
	//8. read data from client & display
	read(cli_fd, msg, sizeof(msg));
	printf("client: %s", msg);

	pthread_mutex_lock(&m);

	ptr = strtok(msg, "/");
	args[i++] = ptr;
	do {
		ptr = strtok(NULL, "/");
		args[i++] = ptr;
	}while(ptr!=NULL);

	handleaction(args);

	printf("pthread_create is called\n");

	}
	pthread_mutex_destroy(&m);
	//12. close socket
	close(cli_fd);
	printf("client socket closed.\n");

	//13. shutdown listening socket
	shutdown(srv_fd, SHUT_RDWR);
	printf("server socket shutdown.\n");
	return 0;	
}


void handelReq(){


	//	pthread_create(&it, NULL, increment, NULL);<F7>



}

void handleaction(char** act){

	printf("act[0] : >>>>%s<<<<\n",act[0]);

	user_t* payload = (user_t*)malloc(sizeof(user_t));

	if(!strcmp("sup",act[0])){

		strcpy(payload->id,act[1]);
		strcpy(payload->pass,act[2]);
		flag = 1;
		pthread_create(&tid[0], NULL, signup,payload);

		pthread_join(tid[0], (void**)&payload);
		printf("Main : >>>>%s<<<<\n",payload->acs);	
		flag = 0;	
		
		write(cli_fd, payload->acs, strlen(payload->acs)+1);

		pthread_mutex_unlock(&m);
		printf("exiting signup\n");	
		//free(payload);

	}

	if(!strcmp("sin",act[0])){

		strcpy(payload->id,act[1]);
		strcpy(payload->pass,act[2]);
		flag = 1;
		pthread_create(&tid[1], NULL, signin,payload);

		pthread_join(tid[1], (void**)&payload);
		printf("Main : >>>>%s<<<<\n",payload->acs);	
		flag = 0;
		
		write(cli_fd, payload->acs, strlen(payload->acs)+1);

		pthread_mutex_unlock(&m);
		printf("exiting signin\n");	

		//free(payload);
	}

	if(!strcmp("MEM",act[0])){

		MemOp(act);

	}
	
//	free(payload);

}

void MemOp(char** act){

	user_t *user = (user_t*)malloc(sizeof(user_t));
	book_t *book = (book_t*)malloc(sizeof(book_t));

	if(!strcmp("E_P",act[2])){
	
		strcpy(user->userid,act[1]);
		strcpy(user->email,act[3]);
		strcpy(user->phone,act[4]);
		pthread_create(&tid[2], NULL, edit_profile, user);

		pthread_join(tid[1], (void**)&user);
		
		write(cli_fd, user->acs, strlen(user->acs)+1);
		pthread_mutex_unlock(&m);
		printf("exiting E_P\n");	
	}

        if(!strcmp("C_P",act[2])){

                strcpy(user->userid,act[1]);
                strcpy(user->pass,act[3]);
                pthread_create(&tid[3], NULL, edit_passwrd, user);

                pthread_join(tid[1], (void**)&user);

                write(cli_fd, user->acs, strlen(user->acs)+1);
                pthread_mutex_unlock(&m);
		printf("exiting C_P\n");	
        }

        if(!strcmp("F_B_B_N",act[2])){

                strcpy(book->userid,act[1]);
                strcpy(book->name,act[3]);
                pthread_create(&tid[4], NULL, find_book_by_name, book);

                pthread_join(tid[1], (void**)&book);

                write(cli_fd, user->acs, strlen(user->acs)+1);
                pthread_mutex_unlock(&m);
		printf("exiting F_B_B_N\n");	

        }

	copy_t* copy = (copy_t*)malloc(sizeof(copy_t));

	if(!strcmp("C_B_A",act[2])){

                strcpy(copy->book_id,act[3]);
                pthread_create(&tid[4], NULL, check_book_avail, copy);

                pthread_join(tid[1], (void**)&copy);

                write(cli_fd, copy->acs, strlen(copy->acs)+1);
                pthread_mutex_unlock(&m);
                printf("exiting C_B_A\n");

        }
/*
	record_t* record = (record_t*)malloc(sizeof(record_t));

        if(!strcmp("L_I_B",act[2])){

                strcpy(record->member_id,act[3]);
                pthread_create(&tid[4], NULL, list_issued_books, record);

                pthread_join(tid[1], (void**)&record);

                write(cli_fd, record->acs, strlen(record->acs)+1);
                pthread_mutex_unlock(&m);
                printf("exiting C_B_A\n");
        }

	free(record);
*/	free(copy);
	free(user);
	free(book);
}




















