#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define COPIES_FILE "copy.txt"

typedef struct copies
{
	char book_id[5];
	char rack[5];
	char acs[20];
	char status[20];  // status = available or not available
}copy_t;

int menu();
int accept_copy(copy_t *c);
int print_copy(copy_t *c);
int add_copy(char *id);
int edit_copy(char *id);
int print_all_copies();
copy_t* find_copy(char *id, copy_t*);
int delete_copy(char *id);
/*
int print_copy(copy_t *c)
{
	printf("%d\n%d\n%d\n%s\n", c->id, c->book_id, c->rack, c->status);
}

int add_copy(char *id)
{
	copy_t c;
	int fd;
	// accept a new book copy from user
	accept_copy(&c);
	// open file for appending into the file
	fd = open(COPIES_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open copy file");
		_exit(1);
	}

	// write copy record into the file
	write(fd, &c, sizeof(c));

	// close file
	close(fd);
}

int print_all_copies()
{
	int fd;
	copy_t c;

	// open file for reading
	fd = open(COPIES_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open user file");
		_exit(1);
	}

	// read a book copy record
	while( read(fd, &c, sizeof(c)) > 0 )
	{
		// display book copy record
		print_copy(&c);
	} // repeat until end of file is reached

	// close file
	close(fd);
}
*/
copy_t* find_copy(char *id,copy_t* copy)
{
	int fd, found=0;
	copy_t c;

	// open file for reading
	fd = open(COPIES_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open copy file");
		_exit(1);
	}

	// read a book copy record
	while( read(fd, &c, sizeof(c)) > 0 )
	{
		if(!strcmp(c.book_id,id))
		{
			// display book copy record
			copy = &c;
			close(fd);
			return copy;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);

}
/*
int edit_copy(char *id)
{
	int fd, id, found=0;
	copy_t c, nc;
	long size = sizeof(struct copies);

	// open file for reading
	fd = open(COPIES_FILE, O_RDWR);
	if(fd < 0)
	{
		perror("failed to open copy file");
		_exit(1);
	}

	printf("enter id to be searched: ");
	scanf("%d", &id);

	// read a book copy record
	while( read(fd, &c, sizeof(c)) > 0 )
	{
		if(c.id == id)
		{
			// display copy record
			print_copy(&c);
			found = 1;

			// enter new details
			accept_copy(&nc);

			// change cur pos to one record back
			lseek(fd, -size, SEEK_CUR);

			// over-write new details
			write(fd, &nc, sizeof(nc));
			break;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);

	if(found == 0)
		printf("copy record not found.\n");
}

int delete_copy(char *id)
{
	int fd, fs, id, found = 0;
	copy_t c;

	// open copy file for reading
	fs = open(COPIES_FILE, O_RDONLY);
	if(fs < 0)
	{
		perror("failed to open copy file");
		_exit(1);
	}

	printf("enter id to be deleted: ");
	scanf("%d", &id);

	// create new temp file for writing
	fd = open("temp.txt", O_WRONLY | O_TRUNC | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open temp file");
		close(fs);
		_exit(1);
	}

	// read a record from copy file
	while( read(fs, &c, sizeof(c)) > 0 )
	{
		if(c.id == id)
			found = 1;
		else
		{
			// write it into temp file, if it is not to be deleted
			write(fd, &c, sizeof(c));
		}
	} // repeat until end of copy file

	// close temp file
	close(fd);
	// close copy file
	close(fs);

	if(found == 0)
		printf("book copy record not found.\n");

	// delete user file
	unlink(COPIES_FILE);
	// rename temp file to copy file
	link("temp.txt", COPIES_FILE);
	unlink("temp.txt");
}
*/

