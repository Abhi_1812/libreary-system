#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define ISSUERECORD_FILE "isseuerecord.txt"

typedef struct issuerecord
{
	int id;
	int copy_id;
	int member_id;
	char issue_date[20];
	char return_duedate[20];
	char return_date[20];
	float fine_amount;
}record_t;

int menu();
void accept_record(record_t *p);
void print_record(record_t *p);
void add_record();
void edit_record();
void print_all_records();
void find_record();
void delete_record();

void print_record(record_t *r)
{
	printf("%d\n%d\n%d\n%s\n%s\n%s\n%f\n", r->id, r->copy_id, r->member_id, r->issue_date, r->return_duedate, r->return_date, r->fine_amount);
}

void add_record()
{
	record_t r;
	int fd;
	// accept a new issue record from user
	accept_record(&r);
	// open file for appending into the file
	fd = open(ISSUERECORD_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open record file");
		_exit(1);
	}

	// write issue record into the file
	write(fd, &r, sizeof(r));

	// close file
	close(fd);
}

void print_all_records()
{
	int fd;
	record_t r;

	// open file for reading
	fd = open(ISSUERECORD_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open record file");
		_exit(1);
	}

	// read a issue record
	while( read(fd, &r, sizeof(r)) > 0 )
	{
		// display issue record
		print_record(&r);
	} // repeat until end of file is reached

	// close file
	close(fd);
}

void find_record()
{
	int fd, id, found=0;
	record_t r;

	// open file for reading
	fd = open(ISSUERECORD_FILE, O_RDONLY);
	if(fd < 0)
	{
		perror("failed to open record file");
		_exit(1);
	}

	printf("enter id to be searched: ");
	scanf("%d", &id);

	// read a user record
	while( read(fd, &r, sizeof(r)) > 0 )
	{
		if(r.id == id)
		{
			// display issue record
			print_record(&r);
			found = 1;
			break;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);

	if(found == 0)
		printf("issue record not found.\n");
}

void edit_record()
{
	int fd, id, found=0;
	record_t r, nr;
	long size = sizeof(struct issuerecord);

	// open file for reading
	fd = open(ISSUERECORD_FILE, O_RDWR);
	if(fd < 0)
	{
		perror("failed to open record file");
		_exit(1);
	}

	printf("enter id to be searched: ");
	scanf("%d", &id);

	// read a issue record
	while( read(fd, &r, sizeof(r)) > 0 )
	{
		if(r.id == id)
		{
			// display issue record
			print_record(&r);
			found = 1;

			// enter new details
			accept_record(&nr);

			// change cur pos to one record back
			lseek(fd, -size, SEEK_CUR);

			// over-write new details
			write(fd, &nr, sizeof(nr));
			break;
		}
	} // repeat until end of file is reached

	// close file
	close(fd);

	if(found == 0)
		printf("issue record not found.\n");
}

void delete_record()
{
	int fd, fs, id, found = 0;
	record_t r;

	// open record file for reading
	fs = open(ISSUERECORD_FILE, O_RDONLY);
	if(fs < 0)
	{
		perror("failed to open record file");
		_exit(1);
	}

	printf("enter id to be deleted: ");
	scanf("%d", &id);

	// create new temp file for writing
	fd = open("temp.txt", O_WRONLY | O_TRUNC | O_CREAT, 0644);
	if(fd < 0)
	{
		perror("failed to open temp file");
		close(fs);
		_exit(1);
	}

	// read a record from record file
	while( read(fs, &r, sizeof(r)) > 0 )
	{
		if(r.id == id)
			found = 1;
		else
		{
			// write it into temp file, if it is not to be deleted
			write(fd, &r, sizeof(r));
		}
	} // repeat until end of user file

	// close temp file
	close(fd);
	// close record file
	close(fs);

	if(found == 0)
		printf("issue record not found.\n");

	// delete record file
	unlink(ISSUERECORD_FILE);
	// rename temp file to user file
	link("temp.txt", ISSUERECORD_FILE);
	unlink("temp.txt");
}


